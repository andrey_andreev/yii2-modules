<?php

namespace reseed\mediaContentManager\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use reseed\mediaContentManager\models\File;
use yii\filters\AccessControl;

/**
 * Class DefaultController
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @package reseed\mediaContentManager\controllers
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @author HieuPhan <hieuphanviet@gmail.com> //modifier
     * @version Ver 1.0 added on 2015.04.05
     * @access public
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!\Yii::$app->getUser()->can('viewFiles')) {
            throw new ForbiddenHttpException();
        }

        $dataProvider = (new File)->search();

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }
}
