<?php
/**
 * Rebecca Application
 *
 * @license    MIT
 * @author     Andreev <andreev1024@gmail.com>
 * @copyright  2015-10-13
 * @link       https://bitbucket.org/reseed/rebecca
 * @version    1.1
 */

namespace reseed\templater\components;

interface TemplaterInterface
{
    public static function getVariableDescription();
}