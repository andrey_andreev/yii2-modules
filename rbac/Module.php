<?php

namespace reseed\rbac;

class Module extends \yii\base\Module
{
    /**
     * translate category for i18n
     * @var string
     */
    public $translateCategory = 'rbac';

    public $controllerNamespace = 'reseed\rbac\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
