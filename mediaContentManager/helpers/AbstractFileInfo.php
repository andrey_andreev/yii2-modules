<?php

namespace reseed\mediaContentManager\helpers;

/**
 * Class AbstractFileInfo
 *
 * @package reseed\mediaContentManager\helpers
 */
abstract class AbstractFileInfo implements FileInfoInterface
{
    /** @var string */
    protected $filename;

    /** @var string */
    protected $name;

    /** @var string */
    protected $extension;

    /** @var int */
    protected $size;

    /** @var string */
    protected $type;

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @param mixed $file
     */
    public function __construct($file)
    {
        $this->init($file);
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access protected
     *
     * @param mixed $file
     */
    abstract protected function init($file);
}
