<?php

namespace reseed\mediaContentManager\helpers;

use yii\web\UploadedFile;

/**
 * Class UploadedFileInfo
 *
 * @author Ivan <frostealth@frostealth.ru>
 * @since 1.0
 *
 * @package reseed\mediaContentManager\helpers
 */
class UploadedFileInfo extends AbstractFileInfo
{
    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @param \yii\web\UploadedFile $file
     */
    public function __construct(UploadedFile $file)
    {
        parent::__construct($file);
    }

    /**
     * @author Ivan <frostealth@frostealth.ru>
     * @version Ver 1.0 added on 2015.05.21
     * @access public
     *
     * @param \yii\web\UploadedFile $file
     */
    protected function init($file)
    {
        $this->filename = $file->tempName;
        $this->name = $file->name;
        $this->extension = $file->getExtension();
        $this->size = $file->size;
        $this->type = $file->type;
    }
}
