<?php
/**
 * Rebecca Application
 *
 * @license    MIT
 * @author     Andreev <andreev1024@gmail.com>
 * @copyright  2015-07-13
 * @link       https://bitbucket.org/reseed/rebecca
 * @version    1.0
 */
namespace reseed\mediaContentManager\models\scopes;

use reseed\mediaContentManager\models\File;
use yii\db\ActiveQuery;

/**
 * Class FileQuery is the Query class for File model.
 */
class FileQuery extends ActiveQuery
{
    /**
     * Add 'image' criteria.
     *
     * @author Andreev <andreev1024@gmail.com>
     * @return $this
     */
    public function image()
    {
        $this->andWhere(['processing_type' => File::PROCESSING_TYPE_IMAGE]);
        return $this;
    }
}
