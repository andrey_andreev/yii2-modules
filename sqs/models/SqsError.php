<?php
/**
 * Rebecca Application
 *
 * @license    MIT
 * @author     Andreev <andreev1024@gmail.com>
 * @copyright  2015-11-02
 * @link       https://bitbucket.org/reseed/rebecca
 * @version    1.1
 */
namespace reseed\sqs\models;

use reseed\sqs\Module;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Model for Sqs errors.
 * @package reseed\sqs
 */
class SqsError extends ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_FIXED = 2;

    const SCENARIO_SEARCH = 'search';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'on' => self::SCENARIO_SEARCH],
            ['sender_fault', 'boolean'],
            [['code', 'message', 'metadata', 'source', 'queue_url'], 'string'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(static::getStatusArray())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sender_fault' => Yii::$app->translate->t('sender fault', Module::$translateCategory),
            'queue_url' => Yii::$app->translate->t('queue url', Module::$translateCategory),
            'code' => Yii::$app->translate->t('code', Module::$translateCategory),
            'message' => Yii::$app->translate->t('message', Module::$translateCategory),
            'status' => Yii::$app->translate->t('status', Module::$translateCategory),
            'metadata' => Yii::$app->translate->t('metadata', Module::$translateCategory),
            'source' => Yii::$app->translate->t('source', Module::$translateCategory),
            'created_at' => Yii::$app->translate->t('created at', Module::$translateCategory),
            'updated_at' => Yii::$app->translate->t('updated at', Module::$translateCategory),
            'created_by' => Yii::$app->translate->t('created by', Module::$translateCategory),
            'updated_by' => Yii::$app->translate->t('updated by', Module::$translateCategory),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SEARCH] = ['id', 'code', 'queue_url'];
        return $scenarios;
    }

    /**
     * Return array with statuses.
     *
     * @author Andreev <andreev1024@gmail.com>
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_NEW => Yii::$app->translate->t('new', Module::$translateCategory),
            self::STATUS_FIXED => Yii::$app->translate->t('fixed', Module::$translateCategory),
        ];
    }

    /**
     * Search method for current model.
     *
     * @author Andreev <andreev1024@gmail.com>
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->scenario = self::SCENARIO_SEARCH;
        
        $query = parent::find();
        $this->load($params);

        $query->andFilterWhere([
            'LIKE', 'code', $this->code,
        ]);

        $query->andFilterWhere([
            'LIKE', 'queue_url', $this->queue_url,
        ]);

        $query->andFilterWhere([
            'IN', 'id', $this->id,
        ]);

        return new ActiveDataProvider(['query' => $query]);
    }
}
